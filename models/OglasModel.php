<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class OglasModel extends Model{
        protected function getFields(): array {
            return [
                'oglas_id'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                'created_at'      => new Field((new \App\Validators\DateTimeValidator())->allowDate()->allowTime() , false),

                'expires_at'      => new Field((new DateTimeValidator())->allowDate()->allowTime()),
                'naziv'           => new Field((new \App\Validators\StringValidator(0, 128)) ),
                'opis'            => new Field((new \App\Validators\StringValidator(0, 128)) ),
                'korisnik_id'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11)),
                'proizvod_id'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11))
            ];
        }

        public function getByNazivOglas(string $naziv){
            return $this->getAllByFieldName('naziv', $naziv);
        }

        public function getByKorisnikId(int $korisnikId) :array{
            return $this->getAllByFieldName('korisnik_id', $korisnikId);
        }

        public function getAllByProizvodId(int $proizvodId): array {
            return $this->getAllByFieldName('proizvod_id', $proizvodId);
        }

        
    }