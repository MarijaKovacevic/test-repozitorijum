<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class ProizvodKarakteristikaModel extends Model{
        protected function getFields(): array {
            return [
                'proizvod_karakteristika_id'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                'created_at'      => new Field((new \App\Validators\DateTimeValidator())->allowDate()->allowTime() , false),

                'value'            => new Field((new \App\Validators\StringValidator(0, 128)) ),
                'opis'            => new Field((new \App\Validators\StringValidator(0, 128)) ),
                'proizvod_id'         => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11)),
                'karakteristika_id'         => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11))
            ];
        }

        public function getByProizvodId(int $proizvodId) :array{
            return $this->getAllByFieldName('proizvod_id', $proizvodId);
        }

        public function getByKarakteristikaId(int $karakteristikaId) :array{
            return $this->getAllByFieldName('karakteristika_id', $karakteristikaId);
        }
    }