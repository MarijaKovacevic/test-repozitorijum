<?php
    namespace App\Controllers;

    class ProizvodController extends \App\Core\Controller{
        

        public function show($id) {
            $proizvodModel = new \App\Models\ProizvodModel($this->getDatabaseConnection());
            $proizvod = $proizvodModel->getById($id);

            if (!$proizvod){
                header ('Location: /');
                exit;
            }
            $this->set('proizvod', $proizvod);
            
            $oglasModel = new \App\Models\OglasModel($this->getDatabaseConnection());
            $oglasiInProizvod = $oglasModel->getAllByProizvodId($id);
            $this->set('oglasiInProizvod',  $oglasiInProizvod);
        }
    }