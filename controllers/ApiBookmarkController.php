<?php
    namespace App\Controllers;

    class ApiBookmarkController extends \App\Core\ApiController {
        public function getBookmarks() {
            $bookmarks = $this->getSession()->get('bookmarks', []);
            $this->set('bookmarks', $bookmarks);
        }

        public function addBookmark($oglasId) {
            $oglasModel = new \App\Models\OglasModel($this->getDatabaseConnection());
            $oglas = $oglasModel->getById($oglasId);

            if (!$oglas) {
                $this->set('error', -1);
                return;
            }

            $bookmarks = $this->getSession()->get('bookmarks', []);

            foreach ($bookmarks as $bookmark) {
                if ($bookmark->oglas_id == $oglasId) {
                    $this->set('error', -2);
                    return;
                }
            }

            $bookmarks[] = $oglas;
            $this->getSession()->put('bookmarks', $bookmarks);

            $this->set('error', 0);
        }

        public function clear() {
            $this->getSession()->put('bookmarks', []);

            $this->set('error', 0);
        }
    }
