<?php
namespace App\Controllers;

class OglasController extends \App\Core\Controller{
    

    public function show($id) { 
        $oglasModel = new \App\Models\OglasModel($this->getDatabaseConnection());
        $oglas = $oglasModel->getById($id);
        $this->set('oglas',  $oglas);
    }

    private function normaliseKeywords(string $keywords): string {
        $keywords = trim($keywords);
        $keywords = preg_replace('/ +/', ' ', $keywords);
    }

    public function postSearch() {
        $oglasModel = new \App\Models\OglasModel($this->getDatabaseConnection());

        $q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);

        $keywords = $this->normaliseKeywords($q);
        
        $Oglas = $OglasModel->getAllBySearch($q);

        print_r($Oglas);
        exit;

        $this->set('Oglas', $Oglas);
        
    }
}