<?php
    namespace App\Controllers;

    class UserOglasManagementController extends \App\Core\Role\UserRoleController {
        public function oglasi(){
            $korisnikId = $this->getSession()->get('korisnik_id');
            $oglasModel = new \App\Models\OglasModel($this->getDatabaseConnection());
            $oglasi = $oglasModel->getByKorisnikId($korisnikId);
            $this->set('oglasi', $oglasi);
        }

        public function getEdit($oglasId) {
            $oglasModel = new \App\Models\OglasModel($this->getDatabaseConnection());
            $oglas = $oglasModel->getById($oglasId);

            if (!$oglas) {
                $this->redirect(\Configuration::BASE . 'user/oglasi');
                return;
            }

            
            if ($oglas->korisnik_id != $this->getSession()->get('korisnik_id')) {
                $this->redirect( \Configuration::BASE . 'user/oglasi' );
                return;
            }

            $offerModel = new \App\Models\offerModel($this->getDatabaseConnection());
            $offer = $offerModel->getAllByOglasId($oglasId);
            if (count($offer) > 0){
                $this->redirect( \Configuration::BASE . 'user/oglasi' );
                return;
            }
            
            $oglas->expires_at = str_replace(' ', 'T', substr($oglas->expires_at, 0, 16));
            print_r($oglas);
            

            $proizvodModel = new \App\Models\ProizvodModel($this->getDatabaseConnection());
            $proizvodi = $proizvodModel->getAll();
            $this->set('proizvodi', $proizvodi);

            $this->set('oglas', $oglas);
            return $oglasModel;
        }

        public function postEdit($oglasId) {
            $this->getEdit($oglasId);

            $editData = [
                'naziv'         => \filter_input(INPUT_POST, 'naziv', FILTER_SANITIZE_STRING),
                'opis'          => \filter_input(INPUT_POST, 'opis', FILTER_SANITIZE_STRING),
                'expires_at'        => \filter_input(INPUT_POST, 'expires_at', FILTER_SANITIZE_STRING),
                'proizvod_id'   => \filter_input(INPUT_POST, 'proizvod_id', FILTER_SANITIZE_NUMBER_INT)
            ];

            $oglasModel = new \App\Models\OglasModel($this->getDatabaseConnection());

            $res = $oglasModel->editById($oglasId, $editData);
            if (!$res) {
                $this->set('message', 'Nije bilo moguce izmeniti oglas.');
                return;
            }

            $uploadStatus = $this->doImageUpload('image', $oglasId);
            if(!$uploadStatus) {
                return;
            }
            
            $this->redirect(\Configuration::BASE . 'user/oglasi');
            
            }

        public function getAdd() {
            $proizvodModel = new \App\Models\ProizvodModel($this->getDatabaseConnection());
            $proizvodi = $proizvodModel->getAll();
            $this->set('proizvodi', $proizvodi);
        }

        public function postAdd() {
            $addData = [
                'naziv'        => \filter_input(INPUT_POST, 'naziv', FILTER_SANITIZE_STRING),
                'opis'         => \filter_input(INPUT_POST, 'opis', FILTER_SANITIZE_STRING),
                'expires_at'   => \filter_input(INPUT_POST, 'expires_at', FILTER_SANITIZE_STRING),
                'proizvod_id'  => \filter_input(INPUT_POST, 'proizvod_id', FILTER_SANITIZE_NUMBER_INT),
                'korisnik_id'  => $this->getSession()->get('korisnik_id')
        ];
        
        $oglasModel = new \App\Models\OglasModel($this->getDatabaseConnection());

        $oglasId = $oglasModel->add($addData);

        if (!$oglasId) {
            $this->set('message', 'Nije dodat oglas.');
            return;
        }

        $uploadStatus = $this->doImageUpload('image', $oglasId);
            if(!$uploadStatus) {
                return;
            }

        $this->redirect( \Configuration::BASE . 'user/oglasi' );
        $this->set('message', 'Doslo je do greske: Nije moguce dodati ovaj oglas!');
        }

        private function doImageUpload(string $fieldName, string $fileName): bool {

            unlink(\Configuration::UPLOAD_DIR . $fileName . '.jpg');
            
            $uploadPath = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            $file = new \Upload\File($fieldName, $uploadPath);
            $file->setName($fileName);
            $file->addValidations([
                new \Upload\Validation\Minetype("image/jpeg"),
                new \Upload\Validation\Size("3M"),
                new \Upload\Validation\Dimentions(320, 240)
            ]);

            try{
                $file-upload();
                return true;
            }catch (\Exception $e){
                $this->set('message', 'Greska: ' . implode(',', $file->getErrors()));
                return false;
            }
        }
    }
