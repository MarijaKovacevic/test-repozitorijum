<?php
    namespace App\Controllers;

    class UserProizvodManagementController extends \App\Core\Role\UserRoleController {
        public function proizvodi(){
            $korisnikId = $this->getSession()->get('korisnik_id');
            $proizvodModel = new \App\Models\ProizvodModel($this->getDatabaseConnection());
            $proizvodi = $proizvodModel->getByKorisnikId($korisnikId);
            $this->set('proizvodi', $proizvodi);
        }

        public function getEdit($proizvodId) {
            $proizvodModel = new \App\Models\ProizvodModel($this->getDatabaseConnection());
            $proizvod = $proizvodModel->getById($proizvodId);

            if (!$proizvod) {
                $this->redirect(\Configuration::BASE . 'user/proizvodi');
            }

            $this->set('proizvod', $proizvod);

            return $proizvodModel;
        }

        public function postEdit($proizvodId) {
            $proizvodModel = $this->getEdit($proizvodId);

            $naziv = filter_input(INPUT_POST, 'naziv', FILTER_SANITIZE_STRING);

            $proizvodModel->editById($proizvodId, [
                'naziv' => $naziv
            ]);

            $this->redirect(\Configuration::BASE . 'user/proizvodi');
        }

        public function getAdd() {

        }

        public function postAdd() {
            $naziv = filter_input(INPUT_POST, 'naziv', FILTER_SANITIZE_STRING);

            $proizvodModel = new \App\Models\ProizvodModel($this->getDatabaseConnection());
            
            $proizvodId = $proizvodModel->add([
                'korisnik_id' => $this->getSession()->get('korisnik_id'),
                'naziv' => $naziv
            ]);

            if ($proizvodId) {
                $this->redirect(\Configuration::BASE . 'user/proizvodi');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati ovaj proizvod!');
        }
    }
