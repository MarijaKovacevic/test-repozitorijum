/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100126
 Source Host           : localhost:3306
 Source Schema         : cvet1

 Target Server Type    : MariaDB
 Target Server Version : 100126
 File Encoding         : 65001

 Date: 13/09/2018 00:19:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for karakteristika
-- ----------------------------
DROP TABLE IF EXISTS `karakteristika`;
CREATE TABLE `karakteristika`  (
  `karakteristika_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `naziv` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`karakteristika_id`) USING BTREE,
  UNIQUE INDEX `uq_karakteristika_naziv`(`naziv`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of karakteristika
-- ----------------------------
INSERT INTO `karakteristika` VALUES (1, '2018-09-05 15:35:34', 'cena');
INSERT INTO `karakteristika` VALUES (2, '2018-09-05 15:35:44', 'vrsta cveca');
INSERT INTO `karakteristika` VALUES (3, '2018-09-05 15:35:57', 'boja');
INSERT INTO `karakteristika` VALUES (4, '2018-09-05 15:36:20', 'zemlja_porekla');

-- ----------------------------
-- Table structure for korisnik
-- ----------------------------
DROP TABLE IF EXISTS `korisnik`;
CREATE TABLE `korisnik`  (
  `korisnik_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `korisnicko_ime` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lozinka` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ime` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prezime` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `salt` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`korisnik_id`) USING BTREE,
  UNIQUE INDEX `uq_korisnik_email`(`email`) USING BTREE,
  UNIQUE INDEX `uq_korisnik_korisnicko_ime`(`korisnicko_ime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of korisnik
-- ----------------------------
INSERT INTO `korisnik` VALUES (1, '2018-09-12 19:42:45', 'Jovan123', '$2y$10$kc9uK.4rlGrlAyPNZ04s3ulGQVPa7aAyNsR3MV6zfr6RuHW575kIi', 'jovan@gmail.com', 'Jovan', 'Jovanovic', 'khkj1150202451521');
INSERT INTO `korisnik` VALUES (2, '2018-09-12 16:48:00', 'Marko52', '$2y$10$kc9uK.4rlGrlAyPNZ04s3ulGQVPa7aAyNsR3MV6zfr6RuHW575kIi', 'marko@gmail.com', 'Marko ', 'Markovic', '125jagjhgshg258kbdajkbc');
INSERT INTO `korisnik` VALUES (3, '2018-09-12 19:42:43', 'Mica25', '$2y$10$kc9uK.4rlGrlAyPNZ04s3ulGQVPa7aAyNsR3MV6zfr6RuHW575kIi', 'mica@gmail.com', 'Milica', 'Milic', '36jhjuueiuhiedhvch5874fg44');
INSERT INTO `korisnik` VALUES (4, '2018-09-12 14:25:49', 'test1234', '$2y$10$kc9uK.4rlGrlAyPNZ04s3ulGQVPa7aAyNsR3MV6zfr6RuHW575kIi', 'test@test.com', 'test1234', 'test1234', '');

-- ----------------------------
-- Table structure for korisnik_prijava
-- ----------------------------
DROP TABLE IF EXISTS `korisnik_prijava`;
CREATE TABLE `korisnik_prijava`  (
  `prijava_korisnik_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `korisnik_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`prijava_korisnik_id`) USING BTREE,
  INDEX `fk_korisnik_prijava_prijava_korisnik_id`(`korisnik_id`) USING BTREE,
  CONSTRAINT `fk_korisnik_prijava_prijava_korisnik_id` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`korisnik_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of korisnik_prijava
-- ----------------------------
INSERT INTO `korisnik_prijava` VALUES (1, '2018-09-05 15:36:44', 1);
INSERT INTO `korisnik_prijava` VALUES (2, '2018-09-05 15:36:48', 2);
INSERT INTO `korisnik_prijava` VALUES (3, '2018-09-05 15:36:58', 3);

-- ----------------------------
-- Table structure for oglas
-- ----------------------------
DROP TABLE IF EXISTS `oglas`;
CREATE TABLE `oglas`  (
  `oglas_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  `naziv` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `opis` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `korisnik_id` int(11) UNSIGNED NOT NULL,
  `proizvod_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`oglas_id`) USING BTREE,
  INDEX `fk_oglas_korisnik_id`(`korisnik_id`) USING BTREE,
  INDEX `fk_oglas_proizvod_id`(`proizvod_id`) USING BTREE,
  CONSTRAINT `fk_oglas_korisnik_id` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`korisnik_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_oglas_proizvod_id` FOREIGN KEY (`proizvod_id`) REFERENCES `proizvod` (`proizvod_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of oglas
-- ----------------------------
INSERT INTO `oglas` VALUES (1, '2018-09-10 15:35:00', '2018-09-26 15:38:47', 'Prodajem cvece za rodjendan', 'Raskosan buket raznovrsnog cveca, kao osecaj velikog zagrljaja', 3, 2);
INSERT INTO `oglas` VALUES (2, '2018-09-11 15:04:15', '2018-09-13 14:43:22', 'Prodajem bidermajere', 'Bidermajer-roze, kajsija, belo u igri pastelnih boja', 1, 7);
INSERT INTO `oglas` VALUES (3, '2018-09-12 13:28:18', '2018-09-29 13:27:52', 'Prodajem cvece za godisnjice', 'Crvene ruze prepletene sa zelenilom i razigranom gipsofilom, zrace jedinstvenim utiskom neobicnosti', 1, 3);
INSERT INTO `oglas` VALUES (5, '2018-09-12 13:28:01', '2018-09-29 13:27:57', 'Prodajem cvece za rodjenja', 'Cvetni aranzman unikatnog oblika, koji nikoga ne ostavlja ravnodusnim.', 2, 5);
INSERT INTO `oglas` VALUES (6, '2018-09-12 13:28:05', '2018-09-29 13:28:02', 'Prodajem sobne biljke', 'Anturium. Smatra se da ima natprirodne moći, cvetovi deluju kao amajlije.', 3, 6);

-- ----------------------------
-- Table structure for proizvod
-- ----------------------------
DROP TABLE IF EXISTS `proizvod`;
CREATE TABLE `proizvod`  (
  `proizvod_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `naziv` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `korisnik_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`proizvod_id`) USING BTREE,
  INDEX `fk_proizvod_korisnik_id`(`korisnik_id`) USING BTREE,
  CONSTRAINT `fk_proizvod_korisnik_id` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`korisnik_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of proizvod
-- ----------------------------
INSERT INTO `proizvod` VALUES (2, '2018-09-12 16:29:25', 'Cvece za rodjendanee', 3);
INSERT INTO `proizvod` VALUES (3, '2018-09-12 16:29:27', 'Cvece za godisnjice', 1);
INSERT INTO `proizvod` VALUES (5, '2018-09-12 16:29:40', 'Cvece za rodjenja', 2);
INSERT INTO `proizvod` VALUES (6, '2018-09-12 16:29:43', 'Sobne biljke', 2);
INSERT INTO `proizvod` VALUES (7, '2018-09-12 16:29:46', 'Cvece za svadbu', 1);
INSERT INTO `proizvod` VALUES (10, '2018-09-13 00:18:58', 'Aranžmani u vazi', 4);

-- ----------------------------
-- Table structure for proizvod_karakteristika
-- ----------------------------
DROP TABLE IF EXISTS `proizvod_karakteristika`;
CREATE TABLE `proizvod_karakteristika`  (
  `proizvod_karakteristika_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `value` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `proizvod_id` int(11) UNSIGNED NOT NULL,
  `karakteristika_id` int(11) UNSIGNED NOT NULL,
  `opis` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`proizvod_karakteristika_id`) USING BTREE,
  UNIQUE INDEX `uq_proizvod_karakteristika_proizvod_id_karakteristika_id`(`proizvod_id`, `karakteristika_id`) USING BTREE,
  INDEX `fk_proizvod_karakteristika_karakteristika_id`(`karakteristika_id`) USING BTREE,
  INDEX `fk_proizvod_karakteristika_proizvod_id`(`proizvod_id`) USING BTREE,
  CONSTRAINT `fk_proizvod_karakteristika_karakteristika_id` FOREIGN KEY (`karakteristika_id`) REFERENCES `karakteristika` (`karakteristika_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_proizvod_karakteristika_proizvod_id` FOREIGN KEY (`proizvod_id`) REFERENCES `proizvod` (`proizvod_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of proizvod_karakteristika
-- ----------------------------
INSERT INTO `proizvod_karakteristika` VALUES (1, '2018-09-07 15:18:32', '800', 5, 1, '');
INSERT INTO `proizvod_karakteristika` VALUES (2, '2018-09-07 15:19:56', '1000', 2, 1, '');
INSERT INTO `proizvod_karakteristika` VALUES (3, '2018-09-07 15:20:03', '900', 3, 1, '');

-- ----------------------------
-- Table structure for slika
-- ----------------------------
DROP TABLE IF EXISTS `slika`;
CREATE TABLE `slika`  (
  `slika_id` int(11) NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `naslov` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `putanja` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `proizvod_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`slika_id`) USING BTREE,
  UNIQUE INDEX `uq_slika_putanja`(`putanja`) USING BTREE,
  INDEX `fk_slika_proizvod_id`(`proizvod_id`) USING BTREE,
  CONSTRAINT `fk_slika_proizvod_id` FOREIGN KEY (`proizvod_id`) REFERENCES `proizvod` (`proizvod_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of slika
-- ----------------------------
INSERT INTO `slika` VALUES (0, '2018-09-05 15:44:54', 'cvet', 'uploads/2018/9/lot-365845.jpg', 2);

SET FOREIGN_KEY_CHECKS = 1;
